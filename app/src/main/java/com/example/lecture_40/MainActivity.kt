package com.example.lecture_40

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.widget.EditText
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var cardsRecyclerViewAdapter: CardsRecyclerViewAdapter
    private lateinit var cards: Array<Array<ItemModel>>


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()
        register()
    }

    private fun init() {
        parseJson()
        cardsRecyclerView.layoutManager = LinearLayoutManager(this)
        cardsRecyclerViewAdapter = CardsRecyclerViewAdapter(cards)
        cardsRecyclerView.adapter = cardsRecyclerViewAdapter
    }

    private fun register() {
        registerButton.setOnClickListener {
            checkRequired()
        }
    }

    private fun checkRequired() {
        //key-value pair map-i
        val info = mutableMapOf<String, String>()
        var toastMessage = "Please fill in:"
        (cards.indices).forEach {
            val card = cards[it]
            (card.indices).forEach {
                val item = card[it]
                if (item.fieldType == "input") {
                    if (item.required && item.text.isNullOrEmpty()) {
                        toastMessage += " ${item.hint},"
                    }
                        info[item.fieldId.toString()] = item.text
                }
            }


        }

        //esaa key-value pair
        d("infoMap", " $info")

        if (toastMessage.isNotEmpty()) {
            //gamoachens im required editText-ebs, romlebic shesavsebia. radgan json-i gameorebulia ramdenimejer, igive hintebs amoagdebs.
            Toast.makeText(this, toastMessage.dropLast(1), Toast.LENGTH_SHORT).show()
        }

    }

    private fun parseJson() {
        cards = Gson().fromJson(jsonString, Array<Array<ItemModel>>::class.java)
    }


    //testirebistvis zomashi gavzarde rom sqrolze gadamemowmebina. aseve icon-is validuri linkebi chavagde
    private val jsonString =
        "[[{\"field_id\":1,\"hint\":\"UserName\",\"field_type\":\"input\",\"keyboard\":\"text\",\"required\":false,\"is_active\":true,\"icon\":\"https://i.ibb.co/CVT41sB/iconfinder-00-ELASTOFONT-STORE-READY-user-circle-2703062-1.png?fbclid=IwAR16f7foK9SfYEGhWvE20zkoMgJFN3s3Kk-waXq_YBtWgVvDhNU-9Vp5BMQ\"},{\"field_id\":2,\"hint\":\"Email\",\"field_type\":\"input\",\"required\":true,\"keyboard\":\"text\",\"is_active\":true,\"icon\":\"https://i.ibb.co/LDKc5J1/email.png\"},{\"field_id\":3,\"hint\":\"phone\",\"field_type\":\"input\",\"required\":true,\"keyboard\":\"number\",\"is_active\":true,\"icon\":\"https://i.ibb.co/jgk9FFR/phone.png\"}],[{\"field_id\":4,\"hint\":\"Full Name\",\"field_type\":\"input\",\"keyboard\":\"text\",\"required\":true,\"is_active\":true,\"icon\":\"https://i.ibb.co/dPZ2zVw/user.png\"},{\"field_id\":14,\"hint\":\"Jemali\",\"field_type\":\"input\",\"keyboard\":\"text\",\"required\":false,\"is_active\":true,\"icon\":\"https://jemala.png\"},{\"field_id\":89,\"hint\":\"Birthday\",\"field_type\":\"chooser\",\"required\":false,\"is_active\":true,\"icon\":\"https://jemala.png\"},{\"field_id\":898,\"hint\":\"Gender\",\"field_type\":\"chooser\",\"required\":\"false\",\"is_active\":true,\"icon\":\"https://jemala.png\"}], [{\"field_id\":20,\"hint\":\"UserName\",\"field_type\":\"input\",\"keyboard\":\"text\",\"required\":false,\"is_active\":true,\"icon\":\"https://i.ibb.co/CVT41sB/iconfinder-00-ELASTOFONT-STORE-READY-user-circle-2703062-1.png?fbclid=IwAR16f7foK9SfYEGhWvE20zkoMgJFN3s3Kk-waXq_YBtWgVvDhNU-9Vp5BMQ\"},{\"field_id\":21,\"hint\":\"Email\",\"field_type\":\"input\",\"required\":true,\"keyboard\":\"text\",\"is_active\":true,\"icon\":\"https://i.ibb.co/LDKc5J1/email.png\"},{\"field_id\":23,\"hint\":\"phone\",\"field_type\":\"input\",\"required\":true,\"keyboard\":\"number\",\"is_active\":true,\"icon\":\"https://i.ibb.co/jgk9FFR/phone.png\"}], [{\"field_id\":24,\"hint\":\"UserName\",\"field_type\":\"input\",\"keyboard\":\"text\",\"required\":false,\"is_active\":true,\"icon\":\"https://i.ibb.co/CVT41sB/iconfinder-00-ELASTOFONT-STORE-READY-user-circle-2703062-1.png?fbclid=IwAR16f7foK9SfYEGhWvE20zkoMgJFN3s3Kk-waXq_YBtWgVvDhNU-9Vp5BMQ\"},{\"field_id\":25,\"hint\":\"Email\",\"field_type\":\"input\",\"required\":true,\"keyboard\":\"text\",\"is_active\":true,\"icon\":\"https://i.ibb.co/LDKc5J1/email.png\"},{\"field_id\":26,\"hint\":\"phone\",\"field_type\":\"input\",\"required\":true,\"keyboard\":\"number\",\"is_active\":true,\"icon\":\"https://i.ibb.co/jgk9FFR/phone.png\"}], [{\"field_id\":27,\"hint\":\"UserName\",\"field_type\":\"input\",\"keyboard\":\"text\",\"required\":false,\"is_active\":true,\"icon\":\"https://i.ibb.co/CVT41sB/iconfinder-00-ELASTOFONT-STORE-READY-user-circle-2703062-1.png?fbclid=IwAR16f7foK9SfYEGhWvE20zkoMgJFN3s3Kk-waXq_YBtWgVvDhNU-9Vp5BMQ\"},{\"field_id\":28,\"hint\":\"Email\",\"field_type\":\"input\",\"required\":true,\"keyboard\":\"text\",\"is_active\":true,\"icon\":\"https://i.ibb.co/LDKc5J1/email.png\"},{\"field_id\":29,\"hint\":\"phone\",\"field_type\":\"input\",\"required\":true,\"keyboard\":\"number\",\"is_active\":true,\"icon\":\"https://i.ibb.co/jgk9FFR/phone.png\"}], [{\"field_id\":30,\"hint\":\"UserName\",\"field_type\":\"input\",\"keyboard\":\"text\",\"required\":false,\"is_active\":true,\"icon\":\"https://i.ibb.co/CVT41sB/iconfinder-00-ELASTOFONT-STORE-READY-user-circle-2703062-1.png?fbclid=IwAR16f7foK9SfYEGhWvE20zkoMgJFN3s3Kk-waXq_YBtWgVvDhNU-9Vp5BMQ\"},{\"field_id\":31,\"hint\":\"Email\",\"field_type\":\"input\",\"required\":true,\"keyboard\":\"text\",\"is_active\":true,\"icon\":\"https://i.ibb.co/LDKc5J1/email.png\"},{\"field_id\":107,\"hint\":\"phone\",\"field_type\":\"input\",\"required\":true,\"keyboard\":\"number\",\"is_active\":true,\"icon\":\"https://i.ibb.co/jgk9FFR/phone.png\"}], [{\"field_id\":32,\"hint\":\"UserName\",\"field_type\":\"input\",\"keyboard\":\"text\",\"required\":false,\"is_active\":true,\"icon\":\"https://i.ibb.co/CVT41sB/iconfinder-00-ELASTOFONT-STORE-READY-user-circle-2703062-1.png?fbclid=IwAR16f7foK9SfYEGhWvE20zkoMgJFN3s3Kk-waXq_YBtWgVvDhNU-9Vp5BMQ\"},{\"field_id\":33,\"hint\":\"Email\",\"field_type\":\"input\",\"required\":true,\"keyboard\":\"text\",\"is_active\":true,\"icon\":\"https://i.ibb.co/LDKc5J1/email.png\"},{\"field_id\":34,\"hint\":\"phone\",\"field_type\":\"input\",\"required\":true,\"keyboard\":\"number\",\"is_active\":true,\"icon\":\"https://i.ibb.co/jgk9FFR/phone.png\"}], [{\"field_id\":35,\"hint\":\"UserName\",\"field_type\":\"input\",\"keyboard\":\"text\",\"required\":false,\"is_active\":true,\"icon\":\"https://i.ibb.co/CVT41sB/iconfinder-00-ELASTOFONT-STORE-READY-user-circle-2703062-1.png?fbclid=IwAR16f7foK9SfYEGhWvE20zkoMgJFN3s3Kk-waXq_YBtWgVvDhNU-9Vp5BMQ\"},{\"field_id\":36,\"hint\":\"Email\",\"field_type\":\"input\",\"required\":true,\"keyboard\":\"text\",\"is_active\":true,\"icon\":\"https://i.ibb.co/LDKc5J1/email.png\"},{\"field_id\":37,\"hint\":\"phone\",\"field_type\":\"input\",\"required\":true,\"keyboard\":\"number\",\"is_active\":true,\"icon\":\"https://i.ibb.co/jgk9FFR/phone.png\"}]]"

}
