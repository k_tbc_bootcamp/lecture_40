package com.example.lecture_40

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.cards_recycler_view_item_layout.view.*

class CardsRecyclerViewAdapter(
    private val cards: Array<Array<ItemModel>>
) :
    RecyclerView.Adapter<CardsRecyclerViewAdapter.ViewHolder>() {

    private lateinit var itemsRecyclerViewAdapter: ItemsRecyclerViewAdapter

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.cards_recycler_view_item_layout, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return cards.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private lateinit var card: Array<ItemModel>

        fun onBind() {
            card = cards[adapterPosition]
            itemView.itemsRecyclerView.layoutManager = LinearLayoutManager(itemView.context)
            itemsRecyclerViewAdapter = ItemsRecyclerViewAdapter(card)
            itemView.itemsRecyclerView.adapter = itemsRecyclerViewAdapter
        }
    }
}