package com.example.lecture_40

import android.R.attr.left
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.graphics.drawable.Drawable
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.util.Log.d
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.CustomTarget
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import kotlinx.android.synthetic.main.button_recycler_view_item_layout.view.*
import kotlinx.android.synthetic.main.edit_text_recycler_view_item_layout.view.*


class ItemsRecyclerViewAdapter(
    private val items: Array<ItemModel>
) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        const val EDIT_TEXT = 0
        const val BUTTON = 1
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        if (viewType == EDIT_TEXT) {
            return EditTextViewHolder(
                LayoutInflater.from(parent.context)
                    .inflate(R.layout.edit_text_recycler_view_item_layout, parent, false)
            )
        }
        return ButtonViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.button_recycler_view_item_layout, parent, false)
        )
    }

    override fun getItemViewType(position: Int): Int {
        val item = items[position]
        if (item.fieldType == "input")
            return EDIT_TEXT
        return BUTTON

    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is EditTextViewHolder) {
            holder.onBind()
        } else if (holder is ButtonViewHolder) {
            holder.onBind()
        }
    }

    inner class EditTextViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private lateinit var item: ItemModel

        fun onBind() {
            item = items[adapterPosition]

            //shevcvalot ID rom register-ze dacheris shemdeg for loopit gadavamowmot required fieldebi carielia tu ara
            //EDIT: vapirebdi mtavar activitshi editText widgetebze mushaobas, magram radgan modelshi shemovige text-i, activityshi modelis textze vamowmeb. tumca ID-is cvlileba mainc davtove.
            itemView.editText.id = item.fieldId

            //gavakete cvladi, rata mivwvde am edittexts
            val editText = itemView.findViewById<EditText>(item.fieldId)

            if (!item.isActive) {
                editText.visibility = View.GONE
            }

            editText.hint = item.hint
            editText.inputType = InputType.TYPE_CLASS_TEXT

            Glide.with(itemView.context)
                .load(item.icon)
                .centerCrop()
                .into(object: CustomTarget<Drawable>() {
                    override fun onLoadCleared(placeholder: Drawable?) {
                    }

                    override fun onResourceReady(
                        resource: Drawable,
                        transition: Transition<in Drawable>?
                    ) {
                        editText.setCompoundDrawablesRelativeWithIntrinsicBounds(null, null, resource, null)
                    }

                })

            editText.addTextChangedListener(object: TextWatcher {
                override fun afterTextChanged(s: Editable?) {

                }

                override fun beforeTextChanged(
                    s: CharSequence?,
                    start: Int,
                    count: Int,
                    after: Int
                ) {

                }

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    item.text = s.toString()
                }

            })

            editText.setText(item.text)

        }
    }

    inner class ButtonViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private lateinit var item: ItemModel

        fun onBind() {
            item = items[adapterPosition]
            itemView.chooserButton.text = item.hint
            //chooser button-s ar vadzlev id-s radgan nikas motxovna iyo, rom funqcionali ar eqneboda
        }
    }
}